﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using NetTopologySuite.Geometries;
using PeliculasAPI.DTOs;
using PeliculasAPI.Models;

namespace PeliculasAPI.Helpers
{
    public class AutoMapperProfiles:Profile
    {
        public AutoMapperProfiles(GeometryFactory geometryFactory)
        {
            #region Mapeando Genero
            CreateMap<Genero, GeneroDTO>().ReverseMap();
            CreateMap<GeneroCreacionDTO, Genero>();
            #endregion

            #region Mapeando Usuario
            CreateMap<IdentityUser, UsuarioDTO>();
            #endregion
            #region Mapeando Actor
            CreateMap<Actor, ActorDTO>().ReverseMap();
            CreateMap<ActorCreacionDTO, Actor>()
                .ForMember(x=>x.Foto, option=>option.Ignore());
            CreateMap<ActorPatchDTO,Actor>().ReverseMap();
            #endregion
            #region Mapeando Pelicula
            CreateMap<Pelicula,PeliculasDTO>().ReverseMap();
            CreateMap<PeliculaCreacionDTO, Pelicula>()
                .ForMember(x=>x.Poster, option => option.Ignore())
                .ForMember(x=>x.PeliculasGeneros, option => option.MapFrom(MapPeliculasGeneros))
                .ForMember(x=>x.PeliculasActores,option => option.MapFrom(MapPeliculasActor));

            CreateMap<PeliculaPatchDTO, Pelicula>().ReverseMap();
            CreateMap<Pelicula, PeliculaDetallesDTO>()
              .ForMember(x => x.Generos, option => option.MapFrom(MapPeliculaGenero))
              .ForMember(x => x.Actores, option => option.MapFrom(MapPeliculaActore));
            #endregion

            #region Mapeando Sala de cine
            CreateMap<SalaDeCine, SalaDeCineDTO>()
                .ForMember(x=>x.Latitud, option =>option.MapFrom(y=>y.Ubicacion.Y))
                .ForMember(x=>x.Longitud, option =>option.MapFrom(x=>x.Ubicacion.X));

            CreateMap<SalaDeCineDTO, SalaDeCine>()
                .ForMember(x=>x.Ubicacion, x=>x.MapFrom(y=>
                geometryFactory.CreatePoint(new Coordinate(y.Longitud, y.Latitud))));

            CreateMap<SalaDeCineCreacionDTO, SalaDeCine>()
                .ForMember(x => x.Ubicacion, x => x.MapFrom(y =>
                geometryFactory.CreatePoint(new Coordinate(y.Longitud, y.Latitud))));
            #endregion

            #region Mapeando Review
            CreateMap<Review, ReviewDTO>()
                .ForMember(x => x.NombreUsuario, y => y.MapFrom(y => y.Usuario.UserName));

            CreateMap<ReviewDTO, Review>();

            CreateMap<ReviewCreacionDTO,Review>();

            #endregion

        }

        private List<ActorPeliculaDetalleDTO> MapPeliculaActore (Pelicula pelicula, PeliculaDetallesDTO peliculaDetalles)
        {
            var resultado = new List<ActorPeliculaDetalleDTO> ();
            if(pelicula.PeliculasActores == null)
            {
                return resultado;
            }

            foreach (var actorPelicula in pelicula.PeliculasActores)
            {
                resultado.Add(new ActorPeliculaDetalleDTO
                {
                    ActorId= actorPelicula.ActorId,
                    Pesonaje = actorPelicula.Personaje,
                    NombrePersona = actorPelicula.Actor.Nombre
                });
            }

            return resultado;
        }

        private List<GeneroDTO> MapPeliculaGenero (Pelicula pelicula, PeliculaDetallesDTO peliculaDetalles)
        {
            var resultado = new List<GeneroDTO>();
            if(pelicula.PeliculasGeneros == null)
            {
                return resultado;
            }

            foreach (var generoPelicula in pelicula.PeliculasGeneros)
            {
                resultado.Add(new GeneroDTO()
                {
                    Id = generoPelicula.GeneroId,
                    Nombre = generoPelicula.Genero.Nombre
                });
            }
            return resultado;
        }

        private List<PeliculasGeneros> MapPeliculasGeneros (PeliculaCreacionDTO peliculaCreacion, Pelicula pelicula)
        {
            var resultado = new List<PeliculasGeneros>();
            if(peliculaCreacion.GenerosIDs == null) { return resultado; }
            foreach (var id in peliculaCreacion.GenerosIDs)
            {
                resultado.Add(new PeliculasGeneros { GeneroId = id });
            }

            return resultado;
        }

        private List<PeliculasActores> MapPeliculasActor(PeliculaCreacionDTO peliculaCreacion, Pelicula pelicula)
        {
            var resultado = new List<PeliculasActores>();

            if(peliculaCreacion.Actores == null) { return resultado; }

            foreach (var actor in peliculaCreacion.Actores)
            {
                resultado.Add(new PeliculasActores() { ActorId = actor.ActorId, Personaje = actor.Personaje });
            }
            return resultado;
        }
    }
}
