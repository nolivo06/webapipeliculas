﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace PeliculasAPI.Helpers
{
    public class FiltroError:ExceptionFilterAttribute
    {
        private readonly ILogger<FiltroError> logger;

        public FiltroError(ILogger<FiltroError> logger)
        {
            this.logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            logger.LogError(context.Exception, context.Exception.Message);

            base.OnException(context);
        }
    }
}
