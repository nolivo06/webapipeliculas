﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PeliculasAPI.DTOs;
using PeliculasAPI.Helpers;
using PeliculasAPI.Migrations;
using PeliculasAPI.Models;
using PeliculasAPI.Servicios;
using System.Linq.Dynamic.Core;

namespace PeliculasAPI.Controllers
{
    [ApiController]
    [Route("api/peliculas")]
    public class PeliculasController : CustomBaseController
    {
        private readonly ApplicationDBContext context;
        private readonly IMapper mapper;
        private readonly IAlmacenadorArchivos almacenador;
        private readonly ILogger<PeliculasController> logger;
        private readonly string contenedor = "peliculas";

        public PeliculasController(ApplicationDBContext context,
            IMapper mapper,
            IAlmacenadorArchivos almacenador,
            ILogger<PeliculasController> logger)
            :base(context,mapper)
        {
            this.context = context;
            this.mapper = mapper;
            this.almacenador = almacenador;
            this.logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<PeliculasIndexDTO>> Get()
        {
            var top = 5;
            var hoy = DateTime.Today;

            var futurosEstrenos = await context.Peliculas
                .Where(x=>x.FechaEstreno > hoy)
                .Take(top)
                .ToListAsync();

            var enCines = await context.Peliculas
                .Where(x=>x.EnCines)
                .Take(top)
                .ToListAsync();

            var resultado = new PeliculasIndexDTO();
            resultado.FuturosEstrenos = mapper.Map<List<PeliculasDTO>>(futurosEstrenos);
            resultado.EnCines = mapper.Map<List<PeliculasDTO>>(enCines);

            return resultado;
    }

        [HttpGet("filtro")]
        public async Task<ActionResult<List<PeliculasDTO>>> Filter([FromQuery] FiltroPeliculaDTO filtroPelicula)
        {
            var peliculaQueryable = context.Peliculas.AsQueryable();
            if (!string.IsNullOrEmpty(filtroPelicula.Titulo))
            {
                peliculaQueryable = peliculaQueryable.Where(x => x.Titulo.Contains(filtroPelicula.Titulo));
            }

            if (filtroPelicula.EnCines)
            {
                peliculaQueryable = peliculaQueryable.Where(x => x.EnCines);
            }

            
            if(filtroPelicula.ProximosEstrenos)
            {
                var hoy = DateTime.Today;
                peliculaQueryable = peliculaQueryable.Where(x => x.FechaEstreno > hoy);
            }

            if(filtroPelicula.GeneroId != 0)
            {
                peliculaQueryable = peliculaQueryable.Where(x => x.PeliculasGeneros
                 .Select(y => y.GeneroId)
                 .Contains(filtroPelicula.GeneroId));
            }

            if (!string.IsNullOrEmpty(filtroPelicula.CampoOrdenar))
            {
                var tipoOrden = filtroPelicula.OrdenAscendente ? "ascending" : "descending";
                try
                {

                    peliculaQueryable = peliculaQueryable.OrderBy($"{filtroPelicula.CampoOrdenar} {tipoOrden}");
                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message, ex);
                }
            }


            await HttpContext.InsertarParametrosPaginacion(peliculaQueryable,
                filtroPelicula.CantidadRegistrosPorPagina);
            var peliculas = await peliculaQueryable.Paginar(filtroPelicula.Paginacion).ToListAsync();

            return mapper.Map<List<PeliculasDTO>>(peliculas);
        }

        [HttpGet("{id}", Name = "obtenerPelicula")]
        public async Task<ActionResult<PeliculaDetallesDTO>> GetById(int id)
        {
            var pelicula = await context.Peliculas
                .Include(x=>x.PeliculasActores).ThenInclude(x=>x.Actor)
                .Include(x=>x.PeliculasGeneros).ThenInclude(x=>x.Genero)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (pelicula == null) { return NotFound(); }

            pelicula.PeliculasActores = pelicula.PeliculasActores.OrderBy(x=>x.Orden).ToList();

            return mapper.Map<PeliculaDetallesDTO>(pelicula);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromForm] PeliculaCreacionDTO peliculaCreacionDTO)
        {
            var pelicula = mapper.Map<Pelicula>(peliculaCreacionDTO);

            if (peliculaCreacionDTO.Poster != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await peliculaCreacionDTO.Poster.CopyToAsync(memoryStream);
                    var contenido = memoryStream.ToArray();
                    var extension = Path.GetExtension(peliculaCreacionDTO.Poster.FileName);
                    pelicula.Poster = await almacenador.GuardarArchivo(contenido, extension, contenedor, peliculaCreacionDTO.Poster.ContentType);
                }
            }
            AsignarOrdenActores(pelicula);
            context.Add(pelicula);
            await context.SaveChangesAsync();

            var peliculaDTO = mapper.Map<PeliculasDTO>(pelicula);
            return new CreatedAtRouteResult("obtenerPelicula", new { id = pelicula.Id }, peliculaDTO);
        }

        private void AsignarOrdenActores(Pelicula pelicula)
        {
            if(pelicula.PeliculasActores != null)
            {
                for(int i =0; i< pelicula.PeliculasActores.Count; i++)
                {
                    pelicula.PeliculasActores[i].Orden = i;
                }
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromForm] PeliculaCreacionDTO peliculaCreacionDTO)
        {
            var peliculaDB = await context.Peliculas
                .Include(x=>x.PeliculasActores)
                .Include(x=>x.PeliculasGeneros)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (peliculaDB == null) { return NotFound(); }

            peliculaDB = mapper.Map(peliculaCreacionDTO, peliculaDB);

            if (peliculaCreacionDTO.Poster != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    await peliculaCreacionDTO.Poster.CopyToAsync(memoryStream);
                    var contenido = memoryStream.ToArray();
                    var extension = Path.GetExtension(peliculaCreacionDTO.Poster.FileName);
                    peliculaDB.Poster = await almacenador.EditarArchivo(contenido, extension, contenedor,
                       peliculaDB.Poster, peliculaCreacionDTO.Poster.ContentType);
                }
            }
            AsignarOrdenActores(peliculaDB);
            await context.SaveChangesAsync();
            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch(int id, JsonPatchDocument<PeliculaPatchDTO> patchDocument)
        {
            return await Patch<Pelicula,PeliculaPatchDTO>(id, patchDocument);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete (int id)
        {
            return await Delete<Pelicula>(id);

        }
    }
}
