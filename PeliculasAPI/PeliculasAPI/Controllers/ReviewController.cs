﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PeliculasAPI.DTOs;
using PeliculasAPI.Helpers;
using PeliculasAPI.Models;
using System.Security.Claims;

namespace PeliculasAPI.Controllers
{
    [Route("api/peliculas/{peliculaId:int}/review")]
    //[ApiController]
    [ServiceFilter(typeof(PeliculaExisteAttribute))]
    public class ReviewController:CustomBaseController
    {
        private readonly ApplicationDBContext context;
        private readonly IMapper mapper;

        public ReviewController(ApplicationDBContext context, IMapper mapper)
            :base(context,mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ReviewDTO>>> Get(int peliculaId
            ,[FromQuery] PaginacionDTO paginacionDTO)
        {
         
            var queryable = context.Reviews.Include(x => x.Usuario).AsQueryable();
            queryable = queryable.Where(x=>x.PeliculaId== peliculaId);
            return await Get<Review, ReviewDTO>(paginacionDTO,queryable);

        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Post(int peliculaId,[FromBody] ReviewCreacionDTO reviewCreacion)
        {
            
            var usuarioId = HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

            var reviewExiste = await context.Reviews.AnyAsync(x=>x.PeliculaId== peliculaId && x.UsuarioId == usuarioId);
            if(reviewExiste)
            {
                return BadRequest("El usuario ya ha escrito un review en esta pelicula");
            }
            var review = mapper.Map<Review>(reviewCreacion);
            review.PeliculaId = peliculaId;
            review.UsuarioId= usuarioId;

            context.Add(review);
            await context.SaveChangesAsync();

            return NoContent();

        }

        [HttpPut("{reviewid:int}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult>Put(int peliculaId, int reviewid, [FromBody] ReviewCreacionDTO reviewCreacion)
        {
           

            var reviewDB = await context.Reviews.FirstOrDefaultAsync(x => x.Id == reviewid);
            if(reviewDB == null)
            {
                return NotFound();
            }

            var usuarioId = HttpContext.User.Claims.FirstOrDefault(x=>x.Type == ClaimTypes.NameIdentifier).Value;
            if(reviewDB.UsuarioId != usuarioId)
            {
                return BadRequest("No tiene permiso para actualizar este review");
            }

            reviewDB = mapper.Map(reviewCreacion, reviewDB);
            await context.SaveChangesAsync();
            return NoContent();
          
        }

        [HttpDelete("{reviewid:int}")]
        [Authorize(AuthenticationSchemes =JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Delete (int reviewid)
        {
            var reviewDB = await context.Reviews.FirstOrDefaultAsync(x=>x.Id==reviewid);
            if(reviewDB == null)
            {
                return NotFound();
            }

            var usuarioId= HttpContext.User.Claims.FirstOrDefault(x=>x.Type== ClaimTypes.NameIdentifier).Value;
            if(reviewDB.UsuarioId != usuarioId)
            {
                return Forbid();

            }

            context.Remove(reviewDB);
            await context.SaveChangesAsync();

            return NoContent();
        }
    }
}
