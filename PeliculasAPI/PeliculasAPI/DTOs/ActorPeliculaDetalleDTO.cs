﻿namespace PeliculasAPI.DTOs
{
    public class ActorPeliculaDetalleDTO
    {
        public int ActorId { get; set; }
        public string Pesonaje { get; set; }
        public string NombrePersona { get; set; }
    }
}
