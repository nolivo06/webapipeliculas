﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PeliculaApiTest
{
    public class UsuarioFaltoFiltro : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            context.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.Email,"example@gmail.com"),
                new Claim(ClaimTypes.Name,"example@gmail.com"),
                new Claim(ClaimTypes.NameIdentifier,"5673b8cf-12de-44f6-92ad-fae4a77932af")
            },"prueba"));

            await next();
        }
    }
}
