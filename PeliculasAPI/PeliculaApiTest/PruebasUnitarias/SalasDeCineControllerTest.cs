﻿using NetTopologySuite;
using NetTopologySuite.Geometries;
using PeliculasAPI.Controllers;
using PeliculasAPI.DTOs;
using PeliculasAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeliculaApiTest.PruebasUnitarias
{
    [TestClass]
    public class SalasDeCineControllerTest:BasePruebas
    {
        [TestMethod]
        public async Task ObtenerSalasDeCinesA5KilometrosOMenos()
        {
            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);

            using (var context = LocalDbDatabaseInitializer.GetDbContextLocalDb(false))
            {
                var salasDeCine = new List<SalaDeCine>()
                {
                    new SalaDeCine {Nombre ="Agora Mall",Ubicacion=geometryFactory.CreatePoint(new Coordinate(-69.9388777, 18.4839233))}
                };

                context.AddRange(salasDeCine);
                await context.SaveChangesAsync();
            }
            var filtro = new SalaDeCineCercanoDTOFiltro()
            {
                distanciaEnKms = 5,
                Latitud = 18.481139,
                Longitud = -69.938950
            };

            using(var context = LocalDbDatabaseInitializer.GetDbContextLocalDb(false))
            {
                var mapper = ConfigurarAutoMapper();
                var controller = new SalaDeCinesController(context,mapper,geometryFactory);
                var respuesta = await controller.Cercanos(filtro);
                var valor = respuesta.Value;
                Assert.AreEqual(3, valor.Count);
            }

        }
    }
}
