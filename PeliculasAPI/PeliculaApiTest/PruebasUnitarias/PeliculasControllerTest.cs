﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using PeliculasAPI.Controllers;
using PeliculasAPI.DTOs;
using PeliculasAPI.Models;
using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeliculaApiTest.PruebasUnitarias
{
    [TestClass]
    public class PeliculasControllerTest:BasePruebas
    {
        private string CrearDataPrueba()
        {
            var databaseName = Guid.NewGuid().ToString();
            var contexto = ConstruirContext(databaseName);
            var genero = new Genero() { Nombre = "Genero1" };

            var pelicula = new List<Pelicula>()
            {
                new Pelicula(){Titulo="Pelicula1",FechaEstreno=new DateTime(2010,1,1),EnCines=false},
                new Pelicula(){Titulo="Pelicula no estrenada",FechaEstreno=DateTime.Today.AddDays(1),EnCines=false},
                new Pelicula(){Titulo="Pelicula en cines",FechaEstreno=DateTime.Today.AddDays(1),EnCines=true}
            };

            var peliculaConGenero = new Pelicula()
            {
                Titulo = "Pelicula con genero",
                FechaEstreno = new DateTime(2010, 1, 1),
                EnCines = false
            };
            pelicula.Add(peliculaConGenero);

            contexto.Add(genero);
            contexto.AddRange(pelicula);
            contexto.SaveChanges();

            var peliculaGenero = new PeliculasGeneros() { GeneroId = genero.Id, PeliculaId = peliculaConGenero.Id };
            contexto.Add(peliculaGenero);
             contexto.SaveChanges();

            return databaseName;

        }

        [TestMethod]
        public async Task FiltrarPorTitulo()
        {
            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();
           
            

            var controller = new PeliculasController(contexto, mapper,null,null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            
            var titulo = "Pelicula1";

            var filtroDTO = new FiltroPeliculaDTO()
            {
                Titulo = titulo,
                CantidadRegistrosPorPagina =10
            };

            var respuesta = await controller.Filter(filtroDTO);
            var peliculas = respuesta.Value;
            Assert.AreEqual(1, peliculas.Count);
            Assert.AreEqual(titulo, peliculas[0].Titulo);
        }

        [TestMethod]
        public async Task FiltrarPorEnCines()
        {
            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();

            var controler = new PeliculasController(contexto,mapper,null,null);
            controler.ControllerContext.HttpContext = new DefaultHttpContext();
             
            var filtroDTO = new FiltroPeliculaDTO()
            {
                EnCines = true
            };

            var respuesta = await controler.Filter(filtroDTO);
            var peliculas = respuesta.Value;
            Assert.AreEqual(1, peliculas.Count);
            Assert.AreEqual("Pelicula en cines", peliculas[0].Titulo);
        }

        [TestMethod]
        public async Task ProximosEstrenos()
        {
            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();    

            var controller = new PeliculasController(contexto,mapper,null,null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var filtroDTO = new FiltroPeliculaDTO()
            {
                ProximosEstrenos= true
            };

            var respuesta = await controller.Filter(filtroDTO);
            var peliculas = respuesta.Value;
            Assert.AreEqual(2, peliculas.Count);
            Assert.AreEqual("Pelicula no estrenada", peliculas[0].Titulo);
        }

        [TestMethod]
        public async Task FiltrarPorGeneroId()
        {
            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();

            var controller = new PeliculasController(contexto, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var generoId = contexto.Generos.Select(x => x.Id).First();

            var filtroDto = new FiltroPeliculaDTO()
            {
                GeneroId = generoId
            };

            var respuesta = await controller.Filter(filtroDto);
            var pelicula = respuesta.Value;
            Assert.AreEqual(1, pelicula.Count);
            Assert.AreEqual("Pelicula con genero", pelicula[0].Titulo);
        }

        [TestMethod]
        public async Task FiltroOrdenarTituloAscendente()
        {
            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();

            var controller = new PeliculasController(contexto, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var filtroDTO = new FiltroPeliculaDTO()
            {
                CampoOrdenar = "titulo",
                OrdenAscendente= true
            };

            var respuesta = await controller.Filter(filtroDTO);
            var peliculas = respuesta.Value;

            var contexto2 = ConstruirContext(nombreDB);
            var peliculasDB = contexto2.Peliculas.OrderBy(x=>x.Titulo).ToList();

            Assert.AreEqual(peliculasDB.Count, peliculas.Count);

            for(var i=0;i<peliculasDB.Count;i++)
            {
                var peliculaDelControlador = peliculas[i];
                var peliculaDB = peliculasDB[i];
                Assert.AreEqual(peliculaDB.Id, peliculaDelControlador.Id);
            }
        }
        [TestMethod]
        public async Task FiltroOrdenarTituloDescendente()
        {

            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();

            var controller = new PeliculasController(contexto, mapper, null, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var filtroDTO = new FiltroPeliculaDTO()
            {
                CampoOrdenar = "titulo",
                OrdenAscendente = false
            };

            var respuesta = await controller.Filter(filtroDTO);
            var peliculas = respuesta.Value;

            var contexto2 = ConstruirContext(nombreDB);
            var peliculasDB = contexto2.Peliculas.OrderByDescending(x => x.Titulo).ToList();

            Assert.AreEqual(peliculasDB.Count, peliculas.Count);

            for (var i = 0; i < peliculasDB.Count; i++)
            {
                var peliculaDelControlador = peliculas[i];
                var peliculaDB = peliculasDB[i];
                Assert.AreEqual(peliculaDB.Id, peliculaDelControlador.Id);
            }
        }

        [TestMethod]
        public async Task FiltroCampoIncorrectoDevuelvePelicula()
        {
            var nombreDB = CrearDataPrueba();
            var contexto = ConstruirContext(nombreDB);
            var mapper = ConfigurarAutoMapper();

            var mock = new Mock<ILogger<PeliculasController>>();

            var controller = new PeliculasController(contexto,mapper,null,mock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var filtroDTO = new FiltroPeliculaDTO()
            {
                CampoOrdenar = "ddfd",
                OrdenAscendente= true
            };

            var respuesta = await controller.Filter(filtroDTO);
            var peliculas = respuesta.Value;

            var contexto2 = ConstruirContext(nombreDB);
            var peliculasDB = contexto2.Peliculas.ToList();

            Assert.AreEqual(peliculasDB.Count, peliculas.Count);
            Assert.AreEqual(1,mock.Invocations.Count);
        }
     }
}
